# An introduction to using Git
## Introduction

This section aims to cover the following topics:
* Version control.
* Git, a popular version control tool.
* How to use Git.

The use of version control tools like Git is universal among professional Software
developers as they enable developers to effectively collaborate in order to produce
high quality source code. Thus mastering such a tool is crucial in your journey to landing
your first patch in a FOSS project.

## What is version control
Before we dive into Git - a version control system (VCS), let us first explain the
concept of version control.

First, imagine a situation where you are writing a script called `my_script.py`,
whatever this script does is arbitrary. Perhaps, it reads in some data then interprets/analyses
this data with some functions that you have written and then creates some pretty graphs.
And now, you want to try and add a new, complicated function that will produce another pretty
graph. Assuming you're a sane person, your first step will likely be to make a copy
of the script from a *version* which you know to be working/stable, just in case
your changes start breaking things. E.g.:

    cp my_script.py my_script_WORKING_VERSION.py

Once you've created this copy, you work day and night on `my_script.py` battling
to add an extra analytical function. Eventually you succeed and your script, with
its new function, works!

But now you want to add another feature... Perhaps this time its some code that
uses your function to create a table of results. Similarly, you will likely
copy your (new) working version of this script:

    cp my_script.py my_script_WORKING_VERSION_WITH_FUNCTION.py

And then you will proceed to edit `my_script.py` until this code works.

Congratulations! You've just manually performed version control!

Of course, this is the simplest case; you have one file (`my_script.py`) and one
developer (you).

Now imagine you're working with a partner. You send `my_script.py` to your partner
and they make some changes, there are now two versions. They send *their* version
back to you, and you implement the changes into your version, there is now one
version again. But what if you made some changes in the time that your partner made
theirs? That means there are three versions. Perhaps your changes conflict with
the changes that they've made, so you're now going to have to spend time figuring
out where these conflicts are.

Now imagine this on a scale where you have multiple files, and multiple developers.
In this scenario (which is very much a reality), the group of developers will want
to maintain a core set of files between themselves that together, form the main
or *working* (but not always...) version of your codebase.

Trying to *control* this with the manual method described above would be a nightmare,
and this is where version control systems like Git can make our lives a lot easier.

### Aside
As an aside, it should be noted that here are many version control systems out
there: SVN, Mercuial, Bazaar and CVS, just to name a few. However, today, Git is
the most widely used modern version control system. It's a mature, actively maintained,
**open source** project, initially developed by Linus Torvalds (the creator of
Linux). In addition to this, Git works well on most operating systems and within
a wide range of IDEs.

## What is Git?
Git is a version control system which tracks the changes of files on your computer
(files which you have asked to be tracked by Git). It's mainly used for source-code
management in software development, but it also has other uses cases.

## When should you use Git?
Git is designed to manage and control versions of source code. Which, in most
languages, simply boils down to files containing lines of text. So really, Git
mainly manages files which have lines of text in. However, it can also be used
to manage other types of files, for example, image files.

Note that Git doesn't care if you're using a file with Python code, a file
containing Javascript, or even a file containing some poetry you may have written,
Git is able to manage and track the versions. But note that if you want Git to track
text files, make sure the file is plain text because some applications (like
Microsoft Word) may appear to be generating plain text, but really they are formatting
applications; meaning that what you write does not end up as 'plain text'.

To check that a file is just 'plain text' we can simply execute: `file my_file.whatever`
which will return `<filename> ASCII text` if the file is just 'plain text'. For
example, this tutorial has been written in markdown, using a text editor:

    $ file tutorials/using_git.md
    tutorials/using_git.md: ASCII text

And thus can managed with git (which it is...).

## How does Git work?
Understanding how Git works is important, because if you're able to understand
the fundamentals of how Git works, then using Git effectively will be much easier.

Understand that Git stores *snapshots* of a project/set of files (unlike many other
VCS systems which store differences).

Imagine a project with three files: A, B and C. The image below shows a VCS that
stores versions of the project by recording *differences* to the files:

![alt text](_images/VCS_differences.png)

This is **not** how Git works. Git thinks of its data more like a series of *snapshots*
of the project's state at a current version. So, with Git, every time you save the
state of your project (i.e. for each version), Git records what *all* of your files
look like at that moment and then stores a reference to that snapshot. Note that, to be
efficient, if files have not changed, Git does not store the file again, it just links
to the previous identical file. Thus versions in Git are a stream of snapshots. The
image below shows this:

![alt text](_images/VCS_snapshots.png)

*Images from: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics

For more information on how Git works, see the
[official documentation](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics).

The next section will be a walkthrough of how we can actually use Git locally.

## Using git
It is a common misconception that to use Git, you need a GitLab/Github account.
However, this is not true. To use Git, we simply need Git installed and a local set
of files within the same directory.

To understand how Git works and how to use Git, we will create a fake project and
manage it under version control. To do this, create a directory called 'scratch'
(`mkdir scratch`), change into this directory and then [`touch`](http://www.linfo.org/touch.html)
the files `FileA`, `FileB` and `FileC` into this directory. If you're unsure about
what you've just done, please revist the [simple commands](simple_commands.md)
tutorial.

Now, let's imagine this 'scratch' directory with our files FileA, FileB and FileC
is a project that we are working on.

At this point in time, these files are in a certain state, i.e. at a certain *version*,
and you can then initialise Git within this project directory with the command:

    git init

Notice that this has now created a (hidden) *.git* directory within your project (`ls -a`
will show this).

Now, you will need to tell Git which files you want to be tracked:

    git add FileA FileB FileC

Alternatively **if you're certain** that you want to add every file and directory
within this project to be tracked: `git add .` can be used as a shortcut.

You can see which files have been modified, added (or not added) or removed with:

    git status

This is a very useful command which you should always use to double check the
current status of your Git project.

Next, we need to make our *initial commit* to the project. Git works by representing
changes to a project as *commits*.

    git commit

This should open up the default text editor of your system where you can write your
commit message then save and exit. Note that it is unlikely that the default text editor
is one you're familiar with. In order to set vim as your editor of choice for Git, execute:
`git config --global core.editor 'vim'`.

Furthermore, it is also possible to completely bypass opening up a text editor if you
wish to leave a short commit message. This can be done with the `-m` option which allows
us to set the commit message from the command line, for example:
`git commit -m 'Initial commit: My first git project'`.

Also note that in Git, the `git commit` command only saves a new commit object in the
*local* repository. If you're hosting your project on a remote server, you'll need to
use `git push` in order to *push* the commits to the server, there will be more
on this later.

You have now sucessfully saved the first *version* of your project.

To view the commit history of a project, execute:

    git log

To exit, `q`.

This shows commits by their *unique* [checksum](https://en.wikipedia.org/wiki/Checksum),
their author, the time and date of the commit and the commit message. Note that everything
in Git is checksummed before it is stored, and then it can be referred to by that *unique* checksum.

Now, let's make some changes to FileA and FileC. With your text editor, open up both
files and add some text. E.g. 'This is a new function in FileA'.

Once complete, execute:

    git diff

This will show changes between the previous commit and the 'working tree'. You should
see that Git has noticed the changes you have made to FileA and FileC.

Now, we need to *stage* the files (with `git add`), so that they will be part of the
next commit. Then, we commit:

    git add FileA FileC
    git commit -m 'Added some text to files: FileA and FileC'

And voilà, we've saved another version of our project! Try `git log` again to see the
commit history.

Hopefully by now you have noticed that commiting versions of the project is just a
repeat of this process. In the next section, we will look at how we can host our
Git-controlled code remotely by using free services like GitLab.
