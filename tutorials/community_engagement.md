# Community Engagement

The nature of Open Source/FOSS software is generally distributed. This means that
the code/people/infrastructure is not hosted/maintained from one central location.
This can present problems which more traditional software development companies,
who are less distributed, may not have had to do deal with. Because of this, communities
have had to develop new ways to communicate so that they can effectively work together
across multiple time zones (asynchronously), geographic locations and even languages.


## Mail

Email may seem like an obvious candidate for communicating in a distributed manner,
but back when many FOSS projects began, email was not really a thing. However as it
became a prominent form of communication,  the concept of *mailing lists* was born.
Mailing lists are essentially a list of email addresses used by an individual/organisation
to send content to multiple recipients at the same time.

The benefits of mailing lists are that they do not require people to be present
in a single time zone, service or client. Additionally, history is preserved and
email can also be encrypted between recipients.

Many FOSS projects use mailing lists to discuss new features, address critical
bugs or devise new project policy. The benefit of doing this is that everyone
involved in that project can easily keep track of the conversation and weigh in
with their opinion on a specific section of the discussion. It is very common to
see a mailing list thread branch off into various sub-discussions.

Mailing lists are still in use today, one of the most notable projects to use them
is, of course, the [Linux Kernel](https://lkml.org/lkml/last100/), this link is
actually both the active mailing list and also an archive of all the emails sent
on the list since the project began, it is truly a testament to how well they work,
when used correctly.


## Chat Systems

As the internet became more capable, projects began to grow and things like internet
forums, which are still common today, began to become a place for software developers to
share ideas. Today, developers use a multitude of real time chat systems, one of
the most famous is Internet Relay Chat (IRC), but you may know other solutions such
as Slack/Discord/Matrix.

IRC is the oldest and most famous in the FOSS world and is a great resource for
teams to collaborate in real-time (synchronously). The most common network used
is [Freenode](https://freenode.net/), which is essentially a large network that
allows people to host their own specific channels.

To connect to IRC there is no ONE client, there are many native/web/mobile clients
you can use to connect to many different networks. We recommend that you use
[IRCCloud](https://www.irccloud.com/) for simplicity, they offer a free account
and have a simple set up process.

Once you have IRC set up, it's a good idea to join some *channels* which interest
you. If, for example, you are working in Python, you may find the channel #python
of use.

In most channels, you will generally find a large community of people who can help
answer your questions and even recommend ways that you can contribute to their
projects. Never be afraid to ask! The Software community is a generally a
very welcoming community and you may notice that there is much distaste
towards unfriendly/unhelpful people within the Software community


## Social Media

As a small note, many FOSS projects now have social media accounts, if you want
to show support for your favourite distro or maybe even follow the updates of
a project you find interesting, go and give them a follow on
Twitter/Facebook/Mastodon/Reddit etc!
