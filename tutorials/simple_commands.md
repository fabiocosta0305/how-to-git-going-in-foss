# Using the command line: Simple commands
## Introducing the CLI
Communication with most modern computers is achieved via simple gestures,
for example, the clicking of a button or the sliding of a slider. These both
employ Graphical User Interfaces (GUIs).

However, most (if not all) operating systems provide an alternative (more
traditional) way of communicating with the computer: the Command Line Interface
(CLI) - a text based interface which allows us to navigate, create, execute,
write or modify a computer's files and directories.


## Using the CLI
Using the command line is simple, first, open up a *Terminal*. For most Linux distributions,
just hit the [*super key*](https://en.wikipedia.org/wiki/Super_key_(keyboard_button))
and search 'Terminal', then press Enter.

Now, for your first command, try typing the following into the *Terminal*:

    $ echo 'I am using the command line!'

You should see something like this:

    $ echo 'I am using the command line!'
    I am using the command line!
    $

The purpose of doing this is to see that we can use the command line to make the
computer 'do something' and then see the results. Here, we've used the command
[*echo*](http://www.linfo.org/echo.html), a command which simply displays a line
of text. Or, more specifically, writes it's arguments to
[standard output](http://www.linfo.org/standard_output.html) (often abbreviated
to *stdout*) - standardised streams of data which are produced by command line
programs.

### Why bother?
At first, using the command line may seem difficult and rather pointless. However,
learning to master the command line will, eventually, greatly improve your
productivity as well as help you to understand the underlying principles of how
computers work.

Bear in mind that as you learn progress, you will often have to use the command
line to, for example, install various libraries/specific software or edit, compile
and run code.

Additionally, learning to use the command line will give you power over the computer.
Communicating with your computer (or a
[remote server](https://www.techopedia.com/definition/3192/remote-access-server-ras))
via the command line allows you to operate with precision, and to take advantage
of a lot of powerful software tools.


## The CLI display
Here is a general example of the *prompt* you will see on a Linux machine:

    [user]@[hostname]:[current directory]$

1. *[user]* represents the current user who is logged in.
    - Execute `whoami`, this should match!
2. *[hostname]* represents the computers name.
3. *[current directory]* is the directory which you are currently working in.
    - Note, you may be used to the word 'Folder' instead of directory. In Linux,
    it's directory.
    - This may just be a tilde (`~`), this represents the home directory
        - Try executing `echo $HOME`
4. `$` is the last piece.

This whole piece of text is known as the *prompt*, or the PS1. Note that this can
customised to display other information. However, just note that whenever you use
a Terminal, you'll be presented with a prompt similar to this.


## Basic Commands
### Current directory

    $ pwd

### List files and directories (of the current working directory)

    $ ls

### Change directory

    $ cd Documents

This will change you into the directory *Documents*, execute `pwd` to see where
you are now.

NOTE: Try to make use of the command line's auto-complete feature. First type,
`cd D` and now double hit `Tab`. This will either auto-complete or list possible
completions.

### Create a directory

    $ mkdir test

This will create a directory called 'test'. You can check its existence with `ls`

NOTE: If you do not want to keep re-typing commands, try using the up and
down arrow keys to cycle through your recent command history.

### Changing into the upper directory
First, change into the 'test' directory with `cd test`. Now, to get back one level
(to 'Documents'):

    cd ..

### Removing a directory
Now let's remove (`rm`) the test directory:

    rm -r test

NOTE: Always be very careful with this command!

We need to add the `-r` option to 'recursively' delete this directory.

### Create an empty file

    touch emptyFile

Now try `ls` to see if its there.

### Copy a file

    cp emptyFile emptyFile2

Now execute `ls` to see the result.

### Move a file
First, create the directory where we are going to move the file to:

    mkdir anotherLocation

Let's move the `emptyFile` there:

    mv emptyFile anotherLocation

Now look inside that directory:

    ls anotherlocation

Now remove the directory:

    rm -r anotherLocation

Now remove the remaining file:

    rm emptyFile2


## Summary of commands
### Common commands

**Command** | **Description**
--- | ---
`cd` | Change directory
`ls` | list files and directories in the current directory
`pwd` | Display the path of the current directory
`touch` | Create an empty file
`mkdir` | Make a directory
`rm` | Remove a file or dir (note that the latter requires the `-r` flag
`cp` | Copy a file or directory
`mv` | Move (or rename) a file or directory
`echo` | Print text to STDOUT
`cat` | Display the contents of a file to STDOUT
`less`| Display the contents of a file in a more interactive way (NOTE: `q` to exit)
`man` | Display documentation about a command

## MAN Pages
This is actually a very powerful feature of using the command line, it actually has a manual built in! If you are exploring new commands or want to try something new about a common command such as *cd*, then you can read the man pages (manual pages).

To use the man pages, you simply type:

    man <command_name>

This will then place you into an interactive manual, where you can scroll in order to read about the different options/features that command offers.

Once you have found what you need you simply type:

    q

This will then close the manual and return you back to the command line

Alternatively, almost all commands implement a `--help` option, which can also be used learn more about
the command in question.

### Cheat sheet
We recommend taking some time to check out the
[command line cheat sheet](https://www.git-tower.com/blog/command-line-cheat-sheet/).


## Exercises
To become familiar with these commands, try to complete all of the following execises.
Don't be afraid to ask on #help if you get stuck.

1. Change to your home directory
    - HINT: The home dir is represented by a tilde (`~`)
2. Create a directory called `exercises`
3. Create a file called `test_file.txt`
4. Navigate to the `exercises/` directory and create `another_test_file.txt`
5. Remove `another_test_file.txt`
6. From the `exercises/` directory, remove `test_file.txt`.
7. Change to your home directory and remove the `exercises/` directory.
8. Create a nested set of directories from your home directory `dir1/dir2/dir3`
    - HINT: You'll need to add another option to the command in order to do this.
9. Create a file called `codethink`
10. Create a directory called `stuff` and move `codethink` to stuff
11. Make a directory called `things` and copy the `stuff/` directory to `things`
12. Change the name of the `things` directory to `importantLesson`
13. See what you've done with `tree importantLesson`
14. Remove this `importantLesson`.

Once you're confident you can execute all of these commands, go ahead and
perform these exercises infront of a fellow codething!

## Further information
For more a more detailed tutorial, checkout [launchschool's tutorial](https://launchschool.com/books/command_line/read/introduction).
