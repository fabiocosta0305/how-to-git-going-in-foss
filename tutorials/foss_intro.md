# Introduction to Free and Open Source Software (FOSS)

## What is open source software?

There's a fundamental distinction between closed-source software (aka, proprietary software) and open-source software. Most people in the general public are much more familiar (or maybe only familiar) with proprietary software, but there are some very important differences.

Windows is a famous example of proprietary or closed-source software. When you use the Windows operating system, you are not provided with any of the source code. This means that you are not free to amend any of the functionality of the software. Similarly, when you install an app on your smart phone, you do not receive the source code.

Open Source software is different. By it's very nature, open source software makes the source code completely available to anyone using the program (hence the name). This is the main principle behind open source: that anyone is free to inspect and modify and use the software freely. Linux based operating systems (such as Fedora, Ubuntu and Debian) are all open-source. The idea came about because some people believed that it was better to build code collaboratively, so they started to make their source code open for other people to change. The most famous example is the Linux kernel, which proved to be a hugely successful project, and changed assumptions about how software could be written. Instead of being written by a dedicated team within a large organisation sat together in an office (as was the traditional way), the Linux kernel was written by many many contributors spread across the world who worked on it in their free time and for their own use-cases or convenience and enjoyment.

## What is Free Software?

Another term often used in this space is Free Software. Although technically a different concept to open source software, the two are often used interchangeably. Free software refers to 'free as in freedom', ie. users having the freedom to inspect/amend/use the software, and does not necessarily refer to the software being used without any monetary cost involved (although this is often the case). Why does this element of freedom matter so much? Let's consider the differences between open source and proprietary in the following case: we know for certain that governmental agencies have exploited and spied on millions of civilians using un-patched security breaches in everyday proprietary software. With open-source software, since anyone is free to look at the source code, there are more eyes looking at the code, so it's much harder for malicious third parties and government agencies to insert surveillance backdoors.  So, when using Windows, you have to trust that Microsoft are not compromising your security and and are doing their best to prevent any severe security flaw. Open-source systems are open to public scrutiny, so there are less chances that security flaws slip through unnoticed.

Together, Free and Open Source Software make up the commonly used acronym FOSS.

## The benefits of FOSS

See the following link: [Benefits of Open Source for you](https://opensource.com/article/18/11/reasons-love-open-source) to learn about the benefits of FOSS.

## Further reading

[opensource.com](https://opensource.com) has some great resources which cover a whole range of open source related topics. For further reading, see the [FAQs and various how-to guides](https://opensource.com/resources).
