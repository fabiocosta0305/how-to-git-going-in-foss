# Package Managers

## What are they, what are they used for?

Like Linux distributions there are a multitude of package managers, and you will inevitably
have to use one at some point. But what is a package manager?

A distribution consists of a large amount of software, some distros more than others. They
generally provide a sane set of default software that has been tested and deemed useful
to the people using the distribution. But of course users require different software for
their different use cases, for example, desktop application developers vs web application
developers.

Expecting users to find the latest source for a specific tool/piece of software,
download it, read the instructions, install the dependencies and then install
the software is just crazy and a waste of time!

To accommodate for all of this, most distributions have a recommended package manager to help users
safely/easily install extra software that they require. Package managers automate the retrieval
of the source files as well as the installation.


## Debian (apt)

The default distribution at Codethink is [Debian Stable](https://www.debian.org/releases/stable/),
although there is nothing stopping you from installing or trying other distros. However, if Debian
is good enough for you, then you should probably get to know the
[Advanced Package tool](https://wiki.debian.org/Ap) - `apt`, which is the default package manager
on all Debian (and also Ubuntu) distros.

`apt` is a command line tool, so to manage packages with it, you must first have access to a
terminal on the system. There are 4 simple tasks when using a package manager: 1) Searching,
2) Installing, 3) Upgrading and 4) Uninstalling packages.

1) Searching

    $ apt search foo

2) Installing

    $ apt install foo

3) Upgrading

    $ apt upgrade foo

4) Uninstalling

    $ apt remove foo


These 4 commands are all you need to find/install/update/remove software on your own
laptop/server.

** Note it is common for package managers to require higher privileges on a system, as they
generally install things into your root partition, therefore, you will have to run
these apt commands using `sudo`. You can read more about `sudo` (here)[https://wiki.debian.org/sudo]

## Other Distros

If you do find yourself on another distro, such as Fedora or Arch, you'll find that their
packages managers `dnf` and `pacman`, respectively, work very much the same way as apt.
You will generally find the documentation for their package managers via the distros
website.

## Installing git

Git is a command line tool which is considered vital by many software engineers.
What git is and how to use it will be covered in a later section of this tutorial,
however, for now, let's ensure that we have git on our machine, try:

    $ git --version

If you have git, you should see something like: `git version 2.19.1` (note that the
numbers may differ), if not, you will see: `bash: git: command not found`. This means
that you do not have git installed, so, to install it;

    $ apt install git

Note that you will probably have to run this command as root (with `sudo`). Also note
that you will have to use the appropriate package manager of your system if you are
not running Debian/Ubuntu.

Now try `git --version` again, and you should see the expected output.

## Flatpak

The Linux desktop has long lacked an app store (like the App store or Play Store). As much
as Linux facilitates development and allows us to easily install software packages like
GCC or node.js, it has never been easy for Linux users to install their favourite music
player (e.g. Spotify) or text editor (e.g. Visual Studio). This is because of the large variety of
distros and package managers, which makes it hard for application developers to choose
which one to develop an application for.

Fast forward to today and we now have [Flatpak](https://flatpak.org/), which is a container
based technology, that allows you to install applications onto any Linux distro, as long
as it has Flatpak itself installed, most distros do, if they don't you can install Flatpak
from their package manager, `apt` for example.

To get started installing apps on your new distro/laptop checkout
Flathub, the app store for Flatpak apps or read this
[guide](https://flatpak.org/setup/).
