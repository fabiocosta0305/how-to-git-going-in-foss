# What is Linux?

This section is intended to discuss the reason that we use Linux at Codethink. As
Codethink is primarily a FOSS company, we prefer to use technologies that are also FOSS.
As you are probably already aware, there are a few popular operating systems that are widely
used across many industries: Linux, Windows & MacOS.


## Unix

Linux has been around since the 1990s and you may hear people discuss terminologies when
using Linux such as POSIX, GNU and Unix. These may be confusing at first, but this section
will hopefully clear this up! Before Linux existed, at AT&T Bell labs the fathers of the C
programming language were also busy inventing a new operating system, they called this
Unix. Unix is actually an operating system, which started out as a research topic,
focusing on multi-tasking abilities and portability among other things. As Unix was
written in C, it became very easy to port across multiple devices, so it became widely
used across a range of industries, such as research, academia and other software
companies.


## Posix

As Unix spread, people began to experiment and add new features to the operating system, new
versions emerged such as the Berkeley Software Distribution, which originally extended
UNIX, but eventually grew into it's own *NIX*, this was at a time when Unix was becoming
ever more popular and large companies like Sun and IBM were also creating their own *NIX*
operating systems, this eventually led to the POSIX standard, which aimed to ensure
software could be ported across multiple Unix distributions.


## Linux

One surprising fact about Unix was that it was actually not Open Source software, it was
license via AT&T, BSD eventually grew so much that people began to develop open source
versions such as FreeBSD, but there still was not a de facto FOSS operating system.

Now this leads to the 1990s, when a very famous free software advocate, began working on
GNU (GNU is not Unix), this person was Richard Stallman. GNU was meant to be an entirely
open source reimplementation/alternative to Unix, Stallman was attempting to develop a new
kernel from scratch, along with a suite of tools to run on top of this kernel, such as
GCC, Emacs etc.

However developing a completely new kernel proved to be no easy task and the development
was slow and never really made any real progress compared with Unix, but the tools that
were developed to run on top of this kernel were actually very useful to many Unix users.
This was when Linus Torvalds, began to develop Linux, which as we know today, is the most
famous open source "operating system", but actually Linux started out as just a kernel. In
order for the Linux kernel to be useful, it needed tools, and in order for the GNU project
to become reality, it needed a kernel, so the Linux operating systems that we know today,
were born from this match made in heaven!


## Distros

These days you do not need to worry much about the history of Linux, but it's still nice to
understand why it's a thing and why at Codethink we prefer this over say a proprietary
operating system, like Windows. However as you may be aware, Linux is not just one single
operating system, it actually comes in many different "flavors" a.k.a distributions, which are
built by many different organisations/communities, to better suit their specific needs/demands.

To understand the differences between distributions, we recommend checking out
[distrowatch.com](https://distrowatch.com/dwres.php?resource=major) and reading about the
different major distributions and trying to understand why they exist.

Any of the major distros are generally safe to use, but if you still feel confused, do not
hesitate to ask people which distro they are using and why - you may have some
interesting conversations.
