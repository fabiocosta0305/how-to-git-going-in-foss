# How to Git going in FOSS
This is guide for those starting out in Software Development with FOSS (Free and
Open Source Software). It is intended for people who do not have much experience
in developer operations and programming.

The guide aims to provide you with training in using essential developer tools
and techniques which, when completed, will ensure that you're ready to land your
first patch in an Open Source Software project.

It is important to note that this guide needn't be read from start to finish.
There may be sections that you are already familiar/competent with, you can just
skip these as you see fit. In addition to this, you should keep in mind that
this guide has been primarily written for those who are new to the world of Open
Source software or lack experience with Git.

## An introduction to Open Source Software
Aims:
1. To learn about the world of Open Source Software

See the introduction to [Free and Open Source Software](tutorials/foss_intro.md).

## An introduction to Linux and Linux Distributions
Aims:
1. An introduction to operating systems.
2. To learn about the abundance of Linux distributions available.

Tutorial here: [Linux Guide](tutorials/linux_guide.md)

## An introduction to FOSS community engagement
Aims:
1. Introduce some common methods of communication in the FOSS world.

Tutorial here: [Community Engagement](tutorials/community_engagement.md)

## Using the command line
### Simple commands
This section will introduce some simple commands that will allow you to read, list
and edit files, as well as navigate the filesystem.

Aims:
1. Introduce the terminal.
2. Become competent in simple command line operations.

Tutorial here: [Simple commands](tutorials/simple_commands.md)

### Installing packages
This section will cover how we can install packages on our system. You will likely
be using Debian Stretch (stable).

Tutoral here: [Package managers](tutorials/package_managers.md)

Aims:
1. To learn about distribution package managers.
2. To install git with your system's package manager.


## Using a text editor to write a simple Python script
Aims:
1. To learn about text editors and how they are different to IDEs.
1. To write a simple Python script within a text editor.
2. To execute the script from the command line.

Tutorial here: [Writing a simple Python script](tutorials/simple_script.md)


## Project 1 (Matrix)
Aims:
1. To learn how (and where) we can host code.
2. Learn how to clone existing projects.
3. To complete a simple project in Python.
4. To learn how to test your code with Pytest

You are now ready to tackle your first Python project. The following two sections
will give a brief overview explaining where we have hosted this project and then provide
a walkthrough showing how you can obtain the project locally on your system.

### Hosting code
A source code repository is a web hosting facility that allows you to store
your source code online. This could simply be a cloud storage service, like
Google Drive. However, most software developers will tend to take advantage of
version control tools/services - the most popular being git, which you will learn
about in [using Git](tutorials/using_git.md).

This training guide is hosted on [GitLab](https://gitlab.com/), a web-based
Git-repository manager which allows you to host source code, under git version
control, for free!

In addition to this, GitLab has many more features such as the issue tracker,
Continuous Integration (CI) support and many more. Such features all contribute
to ensuring that developers can perform their whole operation cycle (the DevOps cycle)
using GitLab.

Proceed to the [first project](https://gitlab.com/ct-starter-guide/project1).


## An introduction to using Git (and GitLab)
Aims:
1. Understand the importance of using Git.
2. Understand why we use a Git manager (e.g. GitLab).
3. Understand the various features offered by GitLab.

Using Git tutorial here: [Using Git](tutorials/using_git.md).

Using GitLab tutorial here: [Using GitLab](tutorials/using_gitlab.md).


## Project 2 (Scrabble Score)

Now that you're comfortable with using `git` and have a good idea about what
GitLab is, you should be ready to tackle the next project. The second project
will be similar to the first, that is, you'll be solving a simple problem with
Python, however, we will be using Git to approach this problem sensibly. Bare
in mind that the purpose of this exercise is to show you how to use `git` and
GitLab to make modifications to *some* source code and how to get those modifications
*merged*.

Aims:
1. Learn to work with with *branches* of the code.
2. Learn how to *merge* branches into the main codebase.
3. Learn more about the review process.

Overview:
* Fork and clone the next project
* Identify and raise an issue.
* Create a new branch to fix it.
* Fix issue, ensuring good commits are used.
* Push the branch.
* Create a Merge Request
* Request Review

Proceed to the [second project](https://gitlab.com/ct-starter-guide/project2).
